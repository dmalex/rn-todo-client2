import React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { Entypo } from '@expo/vector-icons'

export const AppTextBold = props => (
  <Text style={{ ...styles.default, ...props.style }}>{props.children}</Text>
)

const styles = StyleSheet.create({
  default: {
    fontFamily: 'roboto-bold'
  }
})
